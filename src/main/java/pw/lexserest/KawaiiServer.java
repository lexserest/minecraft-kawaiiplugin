package pw.lexserest;

import org.bukkit.plugin.java.JavaPlugin;
import pw.lexserest.database.Database;
import pw.lexserest.database.ExStore;
import pw.lexserest.plugins.login.LoginPlugin;
import pw.lexserest.plugins.game.HungerGame;
import pw.lexserest.utils.Plugin;

import java.nio.file.Path;


public final class KawaiiServer extends JavaPlugin {
    public Database database;
    public ExStore exStore;

//    protected KawaiiServer(JavaPluginLoader loader, PluginDescriptionFile descriptionFile, File dataFolder, File file) {
//        super(loader, descriptionFile, dataFolder, file);
//    }

    @Override
    public void onEnable() {
        Plugin.setPlugin(this);
        try {
            if(!getDataFolder().exists()) {
                getDataFolder().mkdir();
            }
            Path path = Path.of(getDataFolder().getPath(), "database.db");
            //Path path = Path.of("/tmp/", "database.db");
            database = new Database("jdbc:sqlite:" + path.toString());
            exStore = new ExStore();

            initPlugins();
        } catch (Exception e){
            this.getLogger().warning("Error!!! " + e.toString());
            e.printStackTrace();
        } finally {
            this.getLogger().info("Я ЖИВОЙ");
        }
    }

    @Override
    public void onDisable() {
        this.getLogger().info("Я УМЕР");
    }

    private void initPlugins(){
        new LoginPlugin();
        new HungerGame();
    }
}
