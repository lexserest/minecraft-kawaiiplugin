package pw.lexserest.plugins.money;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.KawaiiServer;
import pw.lexserest.utils.Logger;
import pw.lexserest.utils.Plugin;


public class SetMoney implements CommandExecutor {
    private final KawaiiServer plugin;

    public SetMoney(KawaiiServer plugin) {
        this.plugin = plugin;
        plugin.getCommand("setmoney").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        if(args.length == 0) return false;
        double money = 0D;

        try {
            money =  Double.parseDouble(args[0]);
            Plugin.getDatabasePlayer().setPlayerBalance((Player) sender, money);
        } catch (Exception e){
            Logger.warn("ERROR: ", e.toString());
            Logger.warn("Error: ", e.fillInStackTrace().toString());
            return false;
        }

        return true;
    }
}
