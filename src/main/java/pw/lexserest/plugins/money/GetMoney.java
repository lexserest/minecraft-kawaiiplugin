package pw.lexserest.plugins.money;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.KawaiiServer;
import pw.lexserest.utils.Logger;
import pw.lexserest.utils.Plugin;

public class GetMoney implements CommandExecutor {

    public GetMoney() {
        Plugin.getPlugin().getCommand("money").setExecutor(this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player player = (Player) sender;
        try {
            Double balance = Plugin.getDatabasePlayer().getPlayerBalance(player);
            player.sendMessage("Ваш баланс $" + balance.toString());
        } catch (Exception e){
            Logger.warn("ERROR: " + e.toString());
            Logger.warn("Error: " + e.fillInStackTrace());
            return false;
        }
        return true;
    }

}
