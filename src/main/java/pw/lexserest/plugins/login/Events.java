package pw.lexserest.plugins.login;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import pw.lexserest.database.db.models.PlayerDB;
import pw.lexserest.plugins.login.LoginPlugin;
import pw.lexserest.utils.FString;
import pw.lexserest.utils.Plugin;

import java.util.Date;

public class Events implements Listener {
    LoginPlugin loginPlugin;


    public Events(LoginPlugin loginPlugin){
        this.loginPlugin = loginPlugin;
        Plugin.registerEvent(this);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        PlayerDB playerDB = Plugin.getDatabasePlayer().getPlayer(player);
        if(playerDB.location == null) playerDB.setLocation(player.getLocation());
        if(!playerDB.password.isEmpty()){
            if(!playerDB.ip.isEmpty() && playerDB.ip.equals(player.getAddress().getAddress().toString())) {
                // сессия - 1 час
                if (playerDB.last_login.getTime() > new Date().getTime() - 1000 * 60 * 60) {
                    loginPlugin.addLogin(player);
                    return;
                }
            }
        }

        player.sendMessage(
            FString.create(
                "Войдите или зарегистрируйтесь",
                "Используйте &y/login Пароль&rs или &y/reg КодРег Пароль Пароль",
                "&rsДля получения КодРег'а напишите в тг чат - &y@kawaiiserver"
            )
        );
        playerDB.setLocation(player.getLocation());
        playerDB.save();

        createTester(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        if(!loginPlugin.isLogin(player)) {
            player.sendMessage("Войдите или зарегистрируйтесь");
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerCommand(PlayerCommandPreprocessEvent event){
        String msg = event.getMessage();
        if(!loginPlugin.isLogin(event.getPlayer()) && !msg.contains("/login") && !msg.contains("/reg")){
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuitEvent(PlayerQuitEvent event){
        Player player = event.getPlayer();
        if(loginPlugin.isLogin(player)){
            PlayerDB playerDB = Plugin.getDatabasePlayer().getPlayer(player);
            playerDB.last_login = new Date();
            playerDB.setLocation(player.getLocation());
            playerDB.save();
            loginPlugin.logout(player);
        }
    }

    public void createTester(Player player){
        Location location = new Location(
                Plugin.getPlugin().getServer().getWorld("world"),
                0,
                100,
                0,
                -90F,
                -90F
        );
        player.teleport(location);
        player.setGameMode(GameMode.SPECTATOR);
        new BukkitRunnable() {
            @Override
            public void run() {
                if(player.isValid() && player.isOnline()) {
                    if (!loginPlugin.isLogin(player)) {
                        Bukkit.getScheduler().scheduleSyncDelayedTask(Plugin.getPlugin(),
                                () -> player.teleport(location)
                        );
                    } else {
                        cancel();
                    }
                }
                if(!player.isOnline() && player.isValid()){
                    cancel();
                }
            }
        }.runTaskTimerAsynchronously(Plugin.getPlugin(), 20L, 10L);
    }

}
