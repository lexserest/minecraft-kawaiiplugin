package pw.lexserest.plugins.login;

import org.bukkit.entity.Player;
import pw.lexserest.plugins.BasePlugin;
import pw.lexserest.plugins.login.commands.*;
import pw.lexserest.utils.Plugin;

public class LoginPlugin extends BasePlugin {

    public LoginPlugin(){
        this.events();
        this.commands();
    }

    private void events(){
        new Events(this);
    }

    private void commands(){
        new Login(this);
        new Reg(this);
        new Invite(this);
        new ListInvites(this);
        new Pwd(this);
    }

    public boolean isLogin(Player player){
        return Plugin.getRedis().sismember("login", player.getUniqueId().toString());
    }

    public void addLogin(Player player){
        Plugin.getRedis().sadd("login", player.getUniqueId().toString());
    }

    public void logout(Player player){
        Plugin.getRedis().srem("login", player.getUniqueId().toString());
    }
}
