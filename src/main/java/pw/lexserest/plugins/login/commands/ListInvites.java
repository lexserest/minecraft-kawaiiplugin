package pw.lexserest.plugins.login.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.InviteDB;
import pw.lexserest.plugins.login.LoginPlugin;
import pw.lexserest.utils.Logger;
import pw.lexserest.utils.Plugin;

public class ListInvites implements CommandExecutor {

    public ListInvites(LoginPlugin loginPlugin){
        Plugin.registerCommand("reglist", this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (!player.isOp()) return true;

            player.sendMessage("Все коды:");
            player.sendMessage(Plugin.getDatabaseInvite().getAllInvites());
        } else {
            Logger.info("Reg Codes", Plugin.getDatabaseInvite().getAllInvites());
        }
        return true;
    }
}
