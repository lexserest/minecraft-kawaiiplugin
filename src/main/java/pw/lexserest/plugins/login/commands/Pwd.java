package pw.lexserest.plugins.login.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.PlayerDB;
import pw.lexserest.plugins.login.LoginPlugin;
import pw.lexserest.utils.FString;
import pw.lexserest.utils.Plugin;

public class Pwd implements CommandExecutor {
    private final LoginPlugin loginPlugin;

    public Pwd(LoginPlugin loginPlugin){
        this.loginPlugin = loginPlugin;
        Plugin.registerCommand("pwd", this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;

        Player player = (Player) sender;
        PlayerDB playerDB = Plugin.getPlayer(player);

        if(!loginPlugin.isLogin(player)) return true;
        if(args.length < 2) return false;
        if(!args[0].equals(args[1])) return false;

        playerDB.password = args[0];
        playerDB.save();
        player.sendMessage(FString.create("Пароль изменен"));
        return true;
    }
}
