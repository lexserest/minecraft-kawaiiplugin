package pw.lexserest.plugins.login.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.PlayerDB;
import pw.lexserest.plugins.login.LoginPlugin;
import pw.lexserest.utils.FString;
import pw.lexserest.utils.Plugin;

public class Reg implements CommandExecutor {
    private final LoginPlugin loginPlugin;

    public Reg(LoginPlugin loginPlugin){
        this.loginPlugin = loginPlugin;
        Plugin.registerCommand("reg", this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;

        Player player = (Player) sender;
        PlayerDB playerDB = Plugin.getDatabasePlayer().getPlayer(player);

        if(playerDB.location == null) playerDB.setLocation(player.getLocation());
        if(args.length < 3) return false;
        if(!args[1].equals(args[2])) return false;
        if(!Plugin.getDatabaseInvite().getInvite(args[0])) {
            player.sendMessage(FString.create("&rНеверный код авторизации!"));
            return true;
        }

        playerDB.password = args[1];
        playerDB.save();
        player.sendMessage(FString.create("Теперь войдите - &y/login Пароль"));
        return true;
    }
}
