package pw.lexserest.plugins.login.commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.PlayerDB;
import pw.lexserest.plugins.login.LoginPlugin;
import pw.lexserest.utils.FString;
import pw.lexserest.utils.Logger;
import pw.lexserest.utils.Plugin;

import java.util.Date;

public class Login implements CommandExecutor {
    private final LoginPlugin loginPlugin;

    public Login(LoginPlugin loginPlugin){
        this.loginPlugin = loginPlugin;
        Plugin.registerCommand("login", this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;

        Player player = (Player) sender;
        if(loginPlugin.isLogin(player)) return true;

        PlayerDB playerDB = Plugin.getDatabasePlayer().getPlayer(player);

        if(playerDB.password.isEmpty()) {
            player.sendMessage(
                FString.create(
                    "Вы не зарегистрированы...",
                    "Регистрация - &y/reg РегКод Пароль Пароль"
                )
            );
            return true;
        };
        if(args.length < 1) return false;
        if(!playerDB.password.equals(args[0])){
            player.sendMessage(FString.create("&rНеверный пароль"));
            return true;
        }

        this.loginPlugin.addLogin(player);
        player.setGameMode(GameMode.SURVIVAL);
        player.teleport(playerDB.getLocation());
        player.sendMessage(FString.create("Вы вошли. Вы няшка."));

        playerDB.ip = player.getAddress().getAddress().toString();
        playerDB.last_login = new Date();
        playerDB.save();
        Logger.info("Login", player.getDisplayName() + " зашел на сервер");
        return true;
    }
}
