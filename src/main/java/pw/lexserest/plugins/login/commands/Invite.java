package pw.lexserest.plugins.login.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.InviteDB;
import pw.lexserest.plugins.login.LoginPlugin;
import pw.lexserest.utils.Logger;
import pw.lexserest.utils.Plugin;

public class Invite implements CommandExecutor {

    public Invite(LoginPlugin loginPlugin){
        Plugin.registerCommand("reggen", this);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (!player.isOp()) return true;
        }

        String codes = "";
        try {
            if (args.length > 0) {
                int count = Integer.parseInt(args[0]);
                if (count > 0) codes = Plugin.getDatabaseInvite().createInvite(count);
            } else {
                codes = Plugin.getDatabaseInvite().createInvite();
            }
        } catch (NumberFormatException e){
            return false;
        }

        if (sender instanceof Player) {
            Player player = (Player) sender;
            player.sendMessage("Созданы коды авторизации:");
            player.sendMessage(codes);
        } else {
            Logger.info("Invites codes", codes);
        }

        return true;
    }
}
