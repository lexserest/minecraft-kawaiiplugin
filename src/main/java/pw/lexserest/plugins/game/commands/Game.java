package pw.lexserest.plugins.game.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.plugins.game.HungerGame;
import pw.lexserest.utils.Plugin;

public class Game implements CommandExecutor {
    HungerGame plugin;

    public Game(HungerGame plugin){
        Plugin.registerCommand("game", this);
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player player = (Player) sender;

        plugin.addPlayer(player);
        return true;
    }
}