package pw.lexserest.plugins.game.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.lexserest.plugins.game.HungerGame;
import pw.lexserest.utils.Plugin;

public class Exit implements CommandExecutor {
    HungerGame plugin;

    public Exit(HungerGame plugin){
        Plugin.registerCommand("exit", this);
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) return false;
        Player player = (Player) sender;
        if(!plugin.isGame(player)) {
            player.sendMessage("Вы не в игре... /game - войти в игру");
            return true;
        }
        plugin.deadPlayer(player, true);
        return true;
    }
}