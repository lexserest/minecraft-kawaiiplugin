package pw.lexserest.plugins.game;


import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import pw.lexserest.utils.Plugin;

public class Events implements Listener {
    HungerGame plugin;

    public Events(HungerGame plugin){
        Plugin.registerEvent(this);
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent event) {
        Player player = event.getEntity();

        if(player.getWorld().getName().equals("world_game")){
            event.getDrops().removeAll(event.getDrops());
            event.setKeepInventory(true);
            plugin.deadPlayer(player, false);
        }
    }

    @EventHandler
    public void onEntitySpawnEvent(EntitySpawnEvent event){
        if(event.getLocation().getWorld().getName().equals("world_game")){
            if(!event.getEventName().equals("ProjectileLaunchEvent"))
                event.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerRespawnEvent(PlayerRespawnEvent event) {
        Player player = event.getPlayer();
        if(player.getWorld().getName().equals("world_game")){
            Location location = plugin.getLocationPlayer(player);
            event.setRespawnLocation(location);
        }
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        plugin.deadPlayer(player, true);
    }
}

//    @EventHandler(priority = EventPriority.HIGHEST)
//    public void onBlockBreakEvent(BlockBreakEvent event) {
//        Player player = event.getPlayer();
//        if(player.isOp() && player.getGameMode() == GameMode.CREATIVE) return;
//        if(player.getWorld().getName().equals("world_game")){
//            event.setCancelled(true);
//        }
//    }
//
//    @EventHandler(priority = EventPriority.HIGHEST)
//    public void onBlockPlaceEvent(BlockPlaceEvent event) {
//        Player player = event.getPlayer();
//        if(player.isOp() && player.getGameMode() == GameMode.CREATIVE) return;
//        if(player.getWorld().getName().equals("world_game")){
//            event.setCancelled(true);
//        }
//    }
//    @EventHandler(priority = EventPriority.HIGHEST)
//    public void onPlayerBucketEmptyEvent(PlayerBucketEmptyEvent event) {
//        Player player = event.getPlayer();
//        if(player.isOp() && player.getGameMode() == GameMode.CREATIVE) return;
//        if(player.getWorld().getName().equals("world_game")){
//            event.setCancelled(true);
//        }
//    }