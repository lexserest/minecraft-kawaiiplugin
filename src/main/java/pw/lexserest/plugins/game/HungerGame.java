package pw.lexserest.plugins.game;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import pw.lexserest.database.db.seralizators.LocationObj;
import pw.lexserest.plugins.BasePlugin;
import pw.lexserest.plugins.game.commands.Exit;
import pw.lexserest.plugins.game.commands.Game;
import pw.lexserest.utils.FString;
import pw.lexserest.utils.Plugin;
import pw.lexserest.utils.Logger;

import java.util.ArrayList;
import java.util.Objects;

public class HungerGame extends BasePlugin {
    boolean is_game = false;
    ArrayList<Player> players = new ArrayList<>();
    ArrayList<Player> dead = new ArrayList<>();

    public HungerGame() {
        this.createWorld();
        this.commands();
        this.events();
    }

    private void createWorld(){
        WorldCreator wc = new WorldCreator("world_game");
        wc.environment(World.Environment.NORMAL);
        wc.type(WorldType.FLAT);
        wc.generatorSettings();
        World world = wc.createWorld();
        Objects.requireNonNull(world).setDifficulty(Difficulty.HARD);
        world.setSpawnFlags(false, false);
        world.setPVP(false);
    }

    private void commands(){
        new Game(this);
        new Exit(this);
    }

    private void events(){
        new Events(this);
    }

    public boolean isDead(Player player){
        return dead.contains(player);
    }
    public boolean isGame(Player player) {
        return players.contains(player) || dead.contains(player);
    }

    public Location getLocationPlayer(Player player){
        Location location = Objects.requireNonNull(Bukkit.getWorld("world")).getSpawnLocation();
        String locationStr = Plugin.getStore().getString(player, "game_location");
        if(locationStr != null){
            location = Objects.requireNonNull(LocationObj.deserialize(locationStr)).toLocation();
        }
        return location;
    }

    public void addPlayer(Player player){
        if(isDead(player)) {
            player.sendMessage("Вы умерли, ждити следующей голодной игры.");
            return;
        }

        if(isGame(player)){
            player.sendMessage("Вы и так в игре.");
            return;
        }

        player.setGameMode(org.bukkit.GameMode.ADVENTURE);
        LocationObj location = LocationObj.createByLocation(player.getLocation());
        Plugin.getStore().setString(player, "game_location", location.serialize());

        player.teleport(new Location(
                Plugin.getPlugin().getServer().getWorld("world_game"), 0d, 4d, 0d));

        broadcastMsg("&yПришло свежее мясо! Зовут его &g" + player.getDisplayName());
        player.sendMessage("Вы присоединились к \"Голодные игры\"! Удачи!");
        //if(players.size() == 0) reset();
        players.add(player);

        Logger.info("Game", player.getDisplayName() + " зашел в голодные игру");
        Logger.info("Game count", players.size() + " всего пользователей");

        startGame();
    }

    public void deadPlayer(Player player, boolean is_exit){
        dead.add(player);
        players.remove(player);
        String txt = is_exit ? "сбежал" : "был убит";

        player.teleport(getLocationPlayer(player));
        player.setGameMode(org.bukkit.GameMode.SURVIVAL);
        player.sendMessage("Вас убили... Вы вернулись в реальный мир.");

        if(players.size() > 1) {
            broadcastMsg(
                    "&yПользователь &g" + player.getDisplayName() + "&y "+ txt +"!&rs" + "\n" +
                    "Осталось пользователей в игре: &y" + players.size()
            );
        } else {
            broadcastTitle("&yГолодные игры закончены!", "Возврат через 10 секунды!");
            new BukkitRunnable() {
                @Override
                public void run() {
                    players.forEach(p -> {
                        // TODO: получение пользователя
                        p.teleport(getLocationPlayer(p));
                        p.setGameMode(org.bukkit.GameMode.SURVIVAL);
                    });

                    reset();
                    broadcastMsgAll("Заходи повторно в \"Голодные игры\" - /game");
                }
            }.runTaskLater(Plugin.getPlugin(),20 * 10);
        }
    }


    public void broadcastTitle(String title, String msg){
        players.forEach(p ->  p.sendTitle(
                FString.create(title),
                FString.create(msg), 1, 20 * 2, 20)
        );
    }

    public void broadcastMsg(String msg){
        players.forEach(p ->  p.sendMessage(FString.create(msg)));
    }

    public void broadcastMsgAll(String msg){
        Bukkit.getServer().getOnlinePlayers().forEach( p -> {
            if(!p.getWorld().getName().equals("world_game")) p.sendMessage(FString.create(msg));
        });
    }

    public void setPVP(boolean is_pvp){
        Bukkit.getWorld("world_game").setPVP(is_pvp);
    }

    public void reset(){
        setPVP(false);
        players.clear();
        dead.clear();
        is_game = false;
    }

    public void startGame(){
        if(players.size() < 2){
            broadcastMsg("&yГолодные игры начнутся когда будет хотя бы 2 игрока");
            broadcastMsgAll("Заходи в \"Голодные игры\" - /game! Там кто-то есть!");
            return;
        }

        if(is_game) return;
        is_game = true;

        broadcastTitle("&gИгра начнется", "Через 5 сек");

        new BukkitRunnable() {
            @Override
            public void run() {
                setPVP(true);
                broadcastTitle("&gИгра началась!", "Бей, режь, убивай!");
            }
        }.runTaskLater(Plugin.getPlugin(), 20 * 5);

    }
}
