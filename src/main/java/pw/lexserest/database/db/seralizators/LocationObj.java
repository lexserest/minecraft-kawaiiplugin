package pw.lexserest.database.db.seralizators;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.Objects;

public class LocationObj {
    public String world;
    public double x;
    public double y;
    public double z;
    public float pitch;
    public float yaw;

    public LocationObj(){

    }

    public String serialize(){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            System.out.print("LocationObj serialize error: " + e.toString());
        }
        return null;
    }

    @Override
    public String toString(){
        return this.serialize();
    }

    public static LocationObj deserialize(String data){
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(data, LocationObj.class);
        } catch (JsonProcessingException e) {
            System.out.print("LocationObj deserialize error: " + e.toString());
        }
        return null;
    }

    public static LocationObj createByLocation(Location location){
        LocationObj locationObj = new LocationObj();
        locationObj.z = location.getZ();
        locationObj.x = location.getX();
        locationObj.y = location.getY();
        locationObj.yaw = location.getYaw();
        locationObj.pitch = location.getPitch();
        locationObj.world = Objects.requireNonNull(location.getWorld()).getName();
        return locationObj;
    }

    public Location toLocation(){
        return new Location(
                Bukkit.getWorld(this.world),
                this.x,
                this.y,
                this.z,
                this.yaw,
                this.pitch
        );
    }
}
