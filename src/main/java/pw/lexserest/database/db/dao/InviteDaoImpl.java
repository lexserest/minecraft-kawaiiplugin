package pw.lexserest.database.db.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import pw.lexserest.database.db.models.InviteDB;
import pw.lexserest.utils.Logger;
import pw.lexserest.utils.Plugin;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class InviteDaoImpl extends BaseDaoImpl<InviteDB, Long> {
    public InviteDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, InviteDB.class);
        TableUtils.createTableIfNotExists(connectionSource, InviteDB.class);
    }

    public boolean getInvite(String code){
        DeleteBuilder<InviteDB, Long> qBuilder = super.deleteBuilder();

        try {
            qBuilder.where().eq("code", code.trim().toLowerCase());
            return qBuilder.delete() != 0;
        } catch (SQLException e) {
            Logger.info("InviteDao", e.toString());
        }
        return false;
    }

    public String getAllInvites(){
        QueryBuilder<InviteDB, Long> qb = super.queryBuilder();
        String codes = "";
        try {
            List<InviteDB> lists = qb.selectColumns("code").limit(10L).query();
            codes = lists.stream().map(e -> e.code).collect(Collectors.joining(", "));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return codes;
    }

    public String createInvite(){
        InviteDB inviteDB = InviteDB.create();
        try {
            createIfNotExists(inviteDB);
        } catch (SQLException e) {
            Logger.info("InviteDao", e.toString());
        }
        return inviteDB.code;
    }

    public String createInvite(int count){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < count; i++){
            stringBuilder.append(createInvite());
            stringBuilder.append(", ");
        }
        return stringBuilder.toString();
    }
}
