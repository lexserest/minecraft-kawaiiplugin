package pw.lexserest.database.db.dao;

import com.j256.ormlite.dao.Dao;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.PlayerDB;

import java.sql.SQLException;

public interface PlayerDao extends Dao<PlayerDB, Long> {
    public PlayerDB findByUUID(String uuid) throws SQLException;
    public PlayerDB findByPlayer(Player player) throws SQLException;
    public PlayerDB getPlayer(Player player);
    public void setPlayer(PlayerDB playerDB);
}
