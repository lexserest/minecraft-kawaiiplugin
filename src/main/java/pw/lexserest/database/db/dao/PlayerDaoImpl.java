package pw.lexserest.database.db.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.PlayerDB;
import pw.lexserest.utils.Plugin;

import java.sql.SQLException;


public class PlayerDaoImpl extends BaseDaoImpl<PlayerDB, Long> implements PlayerDao {
    public PlayerDaoImpl(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, PlayerDB.class);
        TableUtils.createTableIfNotExists(connectionSource, PlayerDB.class);
    }

    public PlayerDB findByUUID(String uuid) throws SQLException {
        QueryBuilder<PlayerDB, Long> qBuilder = super.queryBuilder();
        return qBuilder.where().eq("uuid", uuid).queryForFirst();
    }

    public PlayerDB findByPlayer(Player player) throws SQLException {
        QueryBuilder<PlayerDB, Long> qBuilder = super.queryBuilder();
        return qBuilder.where().eq("uuid", player.getUniqueId()).queryForFirst();
    }

    public PlayerDB getPlayer(Player player) {
        PlayerDB playerDB = Plugin.getStore().getPlayer(player);
        try {
            if (playerDB == null) {
                playerDB = this.findByPlayer(player);
                if(playerDB != null) {
                    if (playerDB.location == null) playerDB.setLocation(player.getLocation());
                    if (playerDB.ip == null) playerDB.ip = player.getAddress().getAddress().toString();
                }
            }
            if (playerDB == null) {
                playerDB = new PlayerDB(player);
                this.createOrUpdate(playerDB);
            }
            Plugin.getStore().setPlayer(player, playerDB);
            return playerDB;
        } catch (SQLException e){
            Bukkit.getServer().getLogger().warning("[PlayerWrapGet error] " + e.toString());
        }
        return playerDB;
    }


    public void setPlayer(PlayerDB playerDB){
        Bukkit.getScheduler().runTaskAsynchronously(Plugin.getPlugin(), () -> {
            try {
                this.createOrUpdate(playerDB);
                Plugin.getStore().setPlayer(playerDB);
            } catch (SQLException e) {
                Bukkit.getServer().getLogger().warning("[PlayerWrapSet error] " + e.toString());
            }
        });
    }

    public void setPlayerBalance(Player player, Double money){
        PlayerDB playerDB = getPlayer(player);
        playerDB.balance = money;
        playerDB.save();
    }

    public Double getPlayerBalance(Player player){
        PlayerDB playerDB = getPlayer(player);
        return playerDB.balance;
    }
}