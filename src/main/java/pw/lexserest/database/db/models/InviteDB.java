package pw.lexserest.database.db.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import pw.lexserest.database.db.dao.InviteDaoImpl;
import pw.lexserest.libs.RandomString;

import java.util.Date;

@DatabaseTable(tableName = "InviteCode", daoClass = InviteDaoImpl.class)
public class InviteDB {
    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField(unique = true, index = true)
    public String code;

    @DatabaseField(
            dataType = DataType.DATE_STRING,
            columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL",
            readOnly = true,
            canBeNull = false
    )
    private Date date;

    public InviteDB(){
    }

    public static InviteDB create(){
        InviteDB invite = new InviteDB();
        invite.code = RandomString.get(5);
        invite.date = new Date();
        return invite;
    }
}
