package pw.lexserest.database.db.models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.json.JSONObject;
import pw.lexserest.database.db.dao.PlayerDaoImpl;
import pw.lexserest.database.db.seralizators.LocationObj;
import pw.lexserest.utils.Plugin;


@DatabaseTable(tableName = "Player", daoClass = PlayerDaoImpl.class)
public class PlayerDB {

    @DatabaseField(generatedId = true)
    public int id;

    @DatabaseField(unique = true, index = true)
    public UUID uuid;

    @DatabaseField()
    public String nickname;

    @DatabaseField()
    public Double balance;

    @DatabaseField()
    public String password;

    @DatabaseField(
            dataType = DataType.DATE_STRING,
            columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL",
            canBeNull = false,
            format = "yyyy-MM-dd HH:mm:ss"
    )
    public Date last_login;


    @DatabaseField(
            dataType = DataType.DATE_STRING,
            columnDefinition = "DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL",
            canBeNull = false,
            format = "yyyy-MM-dd HH:mm:ss"
    )
    public Date date_reg;

    @DatabaseField
    public String location;

    @DatabaseField
    public String ip;

    public PlayerDB(){
    }

    public void setLocation(Location location){
        this.location = LocationObj.createByLocation(location).toString();
    }

    public Location getLocation(){
        return Objects.requireNonNull(LocationObj.deserialize(this.location)).toLocation();
    }

    public PlayerDB(Player player){
        this.uuid = player.getUniqueId();
        this.nickname = player.getDisplayName();
        this.balance = 0d;
        this.password = "";
        this.last_login = new Date();
        this.date_reg = this.date_reg != null ? this.date_reg : new Date();
        this.ip = player.getAddress().getAddress().toString();
        this.setLocation(player.getLocation());
    }

    public String serialize(){
        JSONObject json = new JSONObject();
        json.put("id", this.id);
        json.put("uuid", this.uuid.toString());
        json.put("nickname", this.nickname);
        json.put("balance", this.balance);
        json.put("password", this.password);
        json.put("last_login", this.last_login.getTime());
        json.put("date_reg", this.date_reg.getTime());
        json.put("location", this.location);
        json.put("ip", this.ip);
        return json.toString();
    }

    public static PlayerDB deserialize(String json) {
        if(json == null) return null;
        JSONObject obj = new JSONObject(json);
        PlayerDB playerDB = new PlayerDB();
        playerDB.id = obj.getInt("id");
        playerDB.uuid = UUID.fromString(obj.getString("uuid"));
        playerDB.nickname = obj.getString("nickname");
        playerDB.password = obj.getString("password");
        playerDB.location = obj.getString("location");
        playerDB.balance = obj.getDouble("balance");
        playerDB.ip = obj.getString("ip");
        try {
            playerDB.last_login = new Date(obj.getLong("last_login"));
            playerDB.date_reg = new Date(obj.getLong("date_reg"));
        } catch (Exception e){
            System.out.print("PlayerDB - Parse Data error");
        }
        return playerDB;
    }

    public void save(){
        Plugin.getDatabasePlayer().setPlayer(this);
    }
}
