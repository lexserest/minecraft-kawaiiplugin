package pw.lexserest.database;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import org.bukkit.Bukkit;
import pw.lexserest.database.db.dao.InviteDaoImpl;
import pw.lexserest.database.db.dao.PlayerDaoImpl;
import pw.lexserest.database.db.models.InviteDB;
import pw.lexserest.database.db.models.PlayerDB;

import java.sql.SQLException;

public final class Database {
    public PlayerDaoImpl player;
    public InviteDaoImpl invite;
    public JdbcPooledConnectionSource connectionSource;
    public ExStore exStore;

    public Database(String databaseUrl) {
        try {
            connectionSource = new JdbcPooledConnectionSource(databaseUrl);
            player = DaoManager.createDao(connectionSource, PlayerDB.class);
            invite = DaoManager.createDao(connectionSource, InviteDB.class);

            exStore = new ExStore();
        } catch (SQLException e) {
            Bukkit.getServer().getLogger().warning("[DATABASE] error " + e.toString());
        }
    }
}
