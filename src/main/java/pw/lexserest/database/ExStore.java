package pw.lexserest.database;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bukkit.entity.Player;
import pw.lexserest.database.db.models.PlayerDB;
import redis.clients.jedis.Jedis;

public final class ExStore {
    public Jedis redis;

    public ExStore(){
        try {
            this.redis = new Jedis();
        } catch (Exception e) {
            System.out.print(e.toString());
        }
    }

    private String getPath(Player player, String key){
        return player.getUniqueId() + "/" + key;
    }

    public String setString(Player player, String key, String data) {
        return redis.set(getPath(player, key), data);
    }
    public String getString(Player player, String key) {
        return redis.get(getPath(player, key));
    }

    public PlayerDB getPlayer(Player player){
        return PlayerDB.deserialize(redis.get(getPath(player, "player")));
    }

    public void setPlayer(Player player, PlayerDB playerDB){
        redis.set(getPath(player, "player"), playerDB.serialize());
    }

    public void setPlayer(PlayerDB playerDB){
        redis.set(playerDB.uuid.toString() + "/" + "player", playerDB.serialize());
    }
}

