package pw.lexserest.utils;

import org.bukkit.Bukkit;

public class Logger {
    public static void info(String name, String ...strings){
        StringBuilder out = new StringBuilder(String.format("[%s] ", name));
        for (String item : strings) {
            out.append(item);
            out.append(" ");
        }
        Bukkit.getServer().getLogger().info(out.toString());
    }

    public static void warn(String name, String ...strings){
        StringBuilder out = new StringBuilder(String.format("[%s] ", name));
        for (String item : strings) out.append(item);
        Bukkit.getServer().getLogger().warning(out.toString());
    }
}
