package pw.lexserest.utils;
import org.bukkit.ChatColor;

public class FString {
    public static String create(String ...string){
        StringBuilder out = new StringBuilder();
        for (String item: string) {
            item = item.replace("&rs", ChatColor.RESET.toString());
            item = item.replace("&y", ChatColor.YELLOW.toString());
            item = item.replace("&r", ChatColor.RED.toString());
            item = item.replace("&g", ChatColor.GOLD.toString());
            out.append(item);
            out.append('\n');
        }
        return out.toString();
    }
}
