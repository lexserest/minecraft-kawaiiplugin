package pw.lexserest.utils;

import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import pw.lexserest.KawaiiServer;
import pw.lexserest.database.Database;
import pw.lexserest.database.ExStore;
import pw.lexserest.database.db.dao.InviteDaoImpl;
import pw.lexserest.database.db.dao.PlayerDaoImpl;
import pw.lexserest.database.db.models.PlayerDB;
import redis.clients.jedis.Jedis;

public class Plugin {
    private static KawaiiServer plugin;

    public static void setPlugin(KawaiiServer plugin){
        Plugin.plugin = plugin;
    }

    public static KawaiiServer getPlugin(){
        return Plugin.plugin;
    }

    public static ExStore getStore(){
        return plugin.exStore;
    }

    public static Jedis getRedis(){
        return plugin.exStore.redis;
    }

    public static Database getDatabases(){
        return plugin.database;
    }

    public static PlayerDaoImpl getDatabasePlayer(){
        return plugin.database.player;
    }
    public static InviteDaoImpl getDatabaseInvite(){
        return plugin.database.invite;
    }

    public static void registerCommand(String name, CommandExecutor clazz){
        Plugin.plugin.getCommand(name).setExecutor(clazz);
    }

    public static void registerEvent(Listener listener){
        Plugin.plugin.getServer().getPluginManager().registerEvents(listener, Plugin.plugin);
    }

    public static PlayerDB getPlayer(Player player){
        return Plugin.getDatabasePlayer().getPlayer(player);
    }

}
